FROM 0xacab.org:4567/leap/gitlab-buildpackage:build_stretch_amd64

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Base debian stretch baseimage with few customisation" Vendor="LEAP" Version="1.x"

ENV DEBIAN_FRONTEND noninteractive

RUN sed -i 's/httpredir/deb/' /etc/apt/sources.list 

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    git \
    leap-archive-keyring \
    locales \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    python \
    ca-certificates \
    sudo \ 
    curl \
    gnupg \
    apt-transport-https && \
    apt-get clean 

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN echo "deb http://deb.leap.se/experimental-platform stretch main" > /etc/apt/sources.list.d/leap.list 
RUN echo 'deb https://apt.dockerproject.org/repo debian-jessie main'> /etc/apt/sources.list.d/docker.list 
RUN curl -s https://apt.dockerproject.org/gpg | apt-key add -
RUN apt-get update 
RUN apt-get -y install docker-engine

RUN id cirunner || adduser --group --system --shell /bin/bash cirunner

COPY files/etc /etc/
